blockregion.module README
=========================



Description
-----------

Some users have complained that block configuration is difficult for 
multi-themes, because the settings for block status, region, weight, and 
throttle need to be made separately for each enabled theme. This simple 
module provides a fix, allowing the optional configuration of blocks for 
all themes at once. Enable this module and go to admin/build/block (the 
bottom).

Project page: <http://drupal.org/project/blockregion>


Notes
-----

XXX


TODO
----

* display $form['all_themes'] and the (second) submit button on top of 
  the form, see code
* show names of "all themes that have the same regions as this theme"


Credits
-------

Nedjo Rogers, nedjo <http://drupal.org/user/4481>
  Original Author, Drupal 4.7 version
Axel Kollmorgen, ax <http://drupal.org/user/8>
  Drupal 5 port, added "copying" of regions (to all themes with the same 
  regions), made scope of settings customizable, cut code to half the size

[feel free to add]
